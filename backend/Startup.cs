﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace backend
{
    public partial class Startup
    {
        public static string _firebaseUrl { get; private set; }
        public static string _firebaseApi { get; private set; }
        public static string _connectionString { get; private set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            _connectionString = Configuration.GetValue<string>("Database:ConnectionString");
            _firebaseUrl = Configuration.GetValue<string>("Firebase:Url");
            _firebaseApi = Configuration.GetValue<string>("Firebase:Api");
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddScoped<HeaderAntiForgeryToken>();
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
            //services.Configure<IdentityOptions>(options =>
            //{
            //    options.Cookies.ApplicationCookie.LoginPath = "/Authen/Login";
            //    options.Cookies.ApplicationCookie.LogoutPath = "/Authen/Signout";
            //    // User settings
            //    //options.User.RequireUniqueEmail = true;
            //});
            services.AddMvc()
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",new Swashbuckle.AspNetCore.Swagger.Info { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePages("application/json", ErrorResponse());
            }
            app.UseSwagger(c=> {
                c.RouteTemplate = "api/swagger/{documentName}/swagger.json";
            });
            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "api/swagger";
                c.SwaggerEndpoint("/api/swagger/v1/swagger.json", "My API V1");
            });
            ConfigureAuth(app);
            app.UseMvc();
        }

        private string ErrorResponse()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { Result = "ERROR", Message = "{0}" });
        }
    }
}
