﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using backend.Models.User;

namespace backend
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class CheckAccessToken
    {
        private readonly RequestDelegate _next;

        public CheckAccessToken(RequestDelegate next)
        {
            _next = next;
        }

        public Task InvokeAsync(HttpContext httpContext)
        {
            if (!httpContext.Request.Cookies.ContainsKey("cat"))
            {
                var user = new Data().LoginWithOAuth(httpContext.User.FindFirst("x").Value, httpContext.User.FindFirst("x").Value).Result;
                if (user["Result"].ToString().Equals("OK"))
                {
                    return _next(httpContext);
                }
                else
                {
                    httpContext.Response.Redirect("/Account/Login");
                }
            }
            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CheckAccessTokenExtensions
    {
        public static IApplicationBuilder UseCheckAccessToken(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CheckAccessToken>();
        }
    }
}
