﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Security.Principal;

namespace backend
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class DeveloperAuth
    {
        private readonly RequestDelegate _next;

        public DeveloperAuth(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            Claim[] claims = new Claim[] {
                new Claim("USER_ID", "DeveloperID"),
                new Claim("USER_EMAIL", "Developer@email.com"),
                new Claim("USER_NAME","DeveloperName"),
                new Claim("TOKEN", "")};
            httpContext.User.AddIdentity(new ClaimsIdentity(new GenericIdentity("DeveloperID", "Token"), claims));
            return _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class DeveloperAuthExtensions
    {
        public static IApplicationBuilder UseDeveloperAuth(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<DeveloperAuth>();
        }
    }
}
