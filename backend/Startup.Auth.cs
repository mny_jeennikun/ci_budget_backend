﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using backend.Models.User;
using Firebase.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace backend
{
    public partial class Startup
    {
        private static readonly string secretKey = "!XC!t$kEY!P@$$KEY";

        private void ConfigureAuth(IApplicationBuilder app)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            string issuer = "connectworkflow.cloud";
            string audience = "user";
            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/api/account/login",
                Issuer = issuer,
                Audience = audience,
                Expiration = TimeSpan.FromDays(1),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity
            });
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = audience,

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters,
                Events = new JwtBearerEvents
                {
                    OnChallenge = context =>
                    {
                        context.Response.Redirect("/Account/Login");
                        context.HandleResponse();

                        return Task.FromResult(0);
                    }
                }
            });

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                AuthenticationScheme = "Cookie",
                CookieName = "access_token",
                LoginPath = "/Account/Login",
                LogoutPath = "/Account/Logout",
                TicketDataFormat = new CustomJwtDataFormat(
                    SecurityAlgorithms.HmacSha256,
                    tokenValidationParameters)
            });
        }

        private async Task<ClaimsIdentity> GetIdentity(string authType, string accessToken)
        {
            var auth = await new Data().LoginWithOAuth(authType, accessToken);
            if (auth["Result"].ToString().Equals("OK"))
            {
                FirebaseAuthLink userData = (FirebaseAuthLink)auth["Auth"];
                Field.User userDetail;
                var getDetail = await new Data().GetUser(userData.User.LocalId);
                if (getDetail["Result"].ToString().Equals("OK"))
                {
                    userDetail = (Field.User)getDetail["Data"];
                    Claim[] claims = new Claim[] {
                            new Claim("USER_ID", userData.User.LocalId),
                            new Claim("USER_EMAIL", userData.User.Email),
                            new Claim("DISPLAY_NAME", userData.User.DisplayName),
                            new Claim("ROLE_ID", userDetail.ROLE_ID),
                            new Claim(ClaimTypes.Role,userDetail.ROLE_ID),
                            new Claim("authType", authType),
                            new Claim("accessToken", accessToken)};
                    return await Task.FromResult(new ClaimsIdentity(new GenericIdentity(userData.User.DisplayName, "Token"), claims));
                }
            }
            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }


    }
}
