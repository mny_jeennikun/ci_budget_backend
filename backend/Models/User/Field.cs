﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.User
{
    public partial class Field
    {
        public class User
        {
            public string USER_ID { get; set; }
            public string FIREBASE_ID { get; set; }
            public string ROLE_ID { get; set; } = "1";
            public string IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }

        public class Login
        {
            public string USER_EMAIL { get; set; }
            public string PASSWORD { get; set; }
        }

        public class CheckAccessToken
        {
            public string AuthType { get; set; }
            public string AccessToken { get; set; }
        }

        public class Register : Login
        {
            public string USER_NAME { get; set; }
            public string FIRST_NAME { get; set; }
            public string LAST_NAME { get; set; }
            public string PASSWORD_CONFIRM { get; set; }
        }
    }
}
