﻿using Firebase.Auth;
using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Firebase.Database.Query;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace backend.Models.User
{
    public class Data
    {
        private ClaimsPrincipal User { get; set; }
        private FirebaseClient Firebase { get; set; }
        private IDbConnection Database { get; set; }
        public Data() => Initial();
        public Data(ClaimsPrincipal user)
        {
            this.User = user;
            Initial(new FirebaseOptions()
            {
                AuthTokenAsyncFactory = () => Task.FromResult(user.FindFirst("TOKEN").Value)
            });
        }
        ~Data()
        {
            this.User = null;
            this.Database = null;
            GC.Collect();
        }

        public async Task<Dictionary<string, object>> Login(Field.Login param)
        {
            Dictionary<string, object> Res = new Dictionary<string, object>();
            try
            {
                var authProvider = new FirebaseAuthProvider(new FirebaseConfig(Startup._firebaseApi));
                FirebaseAuthLink userToken = await authProvider.SignInWithEmailAndPasswordAsync(param.USER_EMAIL, param.PASSWORD);
                Res.Add("Result", "OK");
                Res.Add("Auth", userToken);
            }
            catch (Exception ex)
            {
                Res.Add("Result", "ERROR");
                Res.Add("Message", ex.Message);
            }
            return Res;
        }


        public async Task<Dictionary<string, object>> LoginWithOAuth(string authType, string accessToken)
        {
            Dictionary<string, object> Res = new Dictionary<string, object>();
            try
            {
                int auth = 0;
                switch (authType)
                {
                    case "google.com":
                        auth = (int)FirebaseAuthType.Google;
                        break;
                    case "facebook.com":
                        auth = (int)FirebaseAuthType.Facebook;
                        break;
                    case "password":
                        auth = (int)FirebaseAuthType.EmailAndPassword;
                        break;
                    default:
                        auth = -1;
                        break;
                }

                var authProvider = new FirebaseAuthProvider(new FirebaseConfig(Startup._firebaseApi));
                FirebaseAuthLink userToken = await authProvider.SignInWithOAuthAsync((FirebaseAuthType)auth, accessToken);
                var check = await CheckAccount(userToken.User.LocalId, userToken.User.Email, userToken.User.DisplayName);
                Res.Add("Result", "OK");
                Res.Add("Auth", userToken);
            }
            catch (Exception ex)
            {
                Res.Add("Result", "ERROR");
                Res.Add("Message", ex.Message);
            }
            return Res;
        }

        public async Task<bool> CheckAccount(string firebaseId, string email, string displayName)
        {
            try
            {
                Database.Open();
                var user = await Database.QueryFirstOrDefaultAsync<Field.User>("SELECT TOP 1 * FROM M_USERs WHERE FIREBASE_ID = @FIREBASE_ID", new { FIREBASE_ID = firebaseId });
                if (user != null)
                {
                    return await Task.FromResult(false);
                }
                string ID = Guid.NewGuid().ToString().ToUpper();
                var dbArgs = new DynamicParameters();
                #region insertQuery
                string query = @"INSERT INTO M_USERs
                                           (USER_ID
                                           ,FIREBASE_ID
                                           ,ROLE_ID
                                           ,EMAIL
                                           ,DISPLAY_NAME
                                           ,IS_ACTIVE
                                           ,IS_ACTIVE_DATE
                                           ,DELETE_STATUS
                                           ,CREATE_DATE
                                           ,CREATE_BY
                                           ,UPDATE_DATE
                                           ,UPDATE_BY
                                           ,REVISION)
                                     VALUES
                                         (@USER_ID
                                           ,@FIREBASE_ID
                                           ,@ROLE_ID
                                           ,@EMAIL
                                           ,@DISPLAY_NAME
                                           ,@IS_ACTIVE
                                           ,@IS_ACTIVE_DATE
                                           ,0
                                           ,GETDATE()
                                           ,@CREATE_BY
                                           ,GETDATE()
                                           ,@UPDATE_BY
                                           ,0)";
                dbArgs.Add("@USER_ID", ID);
                dbArgs.Add("@ROLE_ID", "1");
                dbArgs.Add("@FIREBASE_ID", firebaseId);
                dbArgs.Add("@EMAIL", email);
                dbArgs.Add("@DISPLAY_NAME", displayName);
                dbArgs.Add("@IS_ACTIVE", 'Y');
                dbArgs.Add("@IS_ACTIVE_DATE", DateTime.Now);
                dbArgs.Add("@CREATE_BY", ID);
                dbArgs.Add("@UPDATE_BY", ID);
                #endregion
                await Database.ExecuteAsync(query, dbArgs);
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            return await Task.FromResult(true);
        }



        public async Task<Dictionary<string, object>> ForgetPassword(string email)
        {
            Dictionary<string, object> Res = new Dictionary<string, object>();
            try
            {
                var authProvider = new FirebaseAuthProvider(new FirebaseConfig(Startup._firebaseApi));
                await authProvider.SendPasswordResetEmailAsync(email);
                Res.Add("Result", "OK");
            }
            catch (Exception ex)
            {
                Res.Add("Result", "ERROR");
                Res.Add("Message", $"E-mail {email} นี้ไม่มีอยู่ในระบบ {System.Environment.NewLine + ex.Message}");

            }
            return Res;

        }

        public async Task<Dictionary<string, object>> GetUser(string ID)
        {
            Dictionary<string, object> Res = new Dictionary<string, object>();
            try
            {
                var user = await Database.QueryFirstAsync<Field.User>("SELECT * FROM M_USERs WHERE FIREBASE_ID = @FIREBASE_ID", new { FIREBASE_ID = ID });
                Res.Add("Result", "OK");
                Res.Add("Data", user);
            }
            catch (Exception ex)
            {
                Res.Add("Result", "ERROR");
                Res.Add("Message", ex.Message);
            }
            return Res;
        }

        private void Initial(FirebaseOptions option = null)
        {
            if (Database == null)
            {
                Database = new SqlConnection(Startup._connectionString);
            }
            this.Firebase = new FirebaseClient(Startup._firebaseUrl, option);
        }
    }
}
