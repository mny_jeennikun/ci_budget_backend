﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Activity
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.ActivityAdd param)
        {
            if (string.IsNullOrEmpty(param.ACTIVITY_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.ActivityAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_ACTIVITYs WHERE ACTIVITY_CODE = @ACTIVITY_CODE", new { ACTIVITY_CODE = param.ACTIVITY_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Activity Code are duplicate.";
                    return Res;
                }

                # region M_Activity
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_ACTIVITYs]
                                                ([ACTIVITY_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[BRANCH_ID]
                                                ,[SECT_ID]
                                                ,[JOB_ID]
                                                ,[PERIOD_ID]
                                                ,[ACTIVITY_CODE]
                                                ,[ACTIVITY_NAME_TH]
                                                ,[ACTIVITY_NAME_EN]
                                                ,[DESCRIPTION]
                                                ,[TOTAL_BUDGET_AMT]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@ACTIVITY_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,'0'
                                                ,@SECT_ID
                                                ,@JOB_ID
                                                ,@PERIOD_ID
                                                ,@ACTIVITY_CODE
                                                ,@ACTIVITY_NAME_TH
                                                ,@ACTIVITY_NAME_EN
                                                ,@DESCRIPTION
                                                ,@TOTAL_BUDGET_AMT
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ID", ID);
                dbArgs.Add("ACTIVITY_CODE", param.ACTIVITY_CODE);
                dbArgs.Add("ACTIVITY_NAME_TH", param.ACTIVITY_NAME_TH);
                dbArgs.Add("ACTIVITY_NAME_EN", param.ACTIVITY_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("SECT_ID", param.SECT_ID);
                dbArgs.Add("JOB_ID", param.JOB_ID);
                dbArgs.Add("PERIOD_ID", param.PERIOD_ID);
                dbArgs.Add("DESCRIPTION", param.DESCRIPTION);
                dbArgs.Add("TOTAL_BUDGET_AMT", param.TOTAL_BUDGET_AMT);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
            
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;
                #endregion

                #region D_Activity
                for (int i = 0; i < param.Detail.Count; i++)
                {
                    int dupDetail = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM D_ACTIVITYs WHERE ACTIVITY_ITEM_CODE = @ACTIVITY_ITEM_CODE", new { ACTIVITY_ITEM_CODE = param.Detail[i].ACTIVITY_ITEM_CODE });
                    #region D_Activity
                    string IDD = Guid.NewGuid().ToString().ToUpper();
                    #region sqlStringDetail
                    string sqlStringDetail = @"INSERT INTO [dbo].[D_ACTIVITYs]
                                                            ([ACTIVITY_ITEM_ID]
                                                            ,[ACTIVITY_ID]
                                                            ,[SITE_ID]
                                                            ,[SEQ]
                                                            ,[ACTIVITY_ITEM_CODE]
                                                            ,[ACTIVITY_ITEM_NAME]
                                                            ,[ACTIVITY_GROUP_ID]
                                                            ,[BUDGET_CHART_ID]
                                                            ,[BUDGET_AMT]
                                                            ,[DELETE_STATUS]
                                                            ,[CREATE_DATE]
                                                            ,[CREATE_BY]
                                                            ,[UPDATE_DATE]
                                                            ,[UPDATE_BY]
                                                            ,[REVISION])
                                                       VALUES
                                                            (@ACTIVITY_ITEM_ID
                                                            ,@ACTIVITY_ID
                                                            ,@SITE_ID
                                                            ,@SEQ
                                                            ,@ACTIVITY_ITEM_CODE
                                                            ,@ACTIVITY_ITEM_NAME
                                                            ,@ACTIVITY_GROUP_ID
                                                            ,@BUDGET_CHART_ID
                                                            ,@BUDGET_AMT
                                                            ,0
                                                            ,GETDATE()
                                                            ,@USER_ID
                                                            ,GETDATE()
                                                            ,@USER_ID
                                                            ,0)";
                    #endregion
                    var dbArgsD = new DynamicParameters();
                    dbArgsD.Add("ACTIVITY_ITEM_ID", IDD);
                    dbArgsD.Add("ACTIVITY_ID", ID);
                    dbArgsD.Add("SITE_ID", param.SITE_ID);
                    dbArgsD.Add("SEQ", param.Detail[i].SEQ);
                    dbArgsD.Add("ACTIVITY_ITEM_CODE", param.Detail[i].ACTIVITY_ITEM_CODE);
                    dbArgsD.Add("ACTIVITY_ITEM_NAME", param.Detail[i].ACTIVITY_ITEM_NAME);
                    dbArgsD.Add("ACTIVITY_GROUP_ID", param.Detail[i].ACTIVITY_GROUP_ID);
                    dbArgsD.Add("BUDGET_CHART_ID", param.Detail[i].BUDGET_CHART_ID);
                    dbArgsD.Add("BUDGET_AMT", param.Detail[i].BUDGET_AMT);
                    dbArgsD.Add("USER_ID", User.FindFirstValue("USER_ID"));
                    await Database.ExecuteAsync(sqlStringDetail, dbArgsD);
                    Res.Result = "OK";
                    Res.Data = IDD;

                    #endregion
                }
                #endregion




            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> AddGroup(Field.ActivityAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region -------
                //string code = @"SELECT A.ACTIVITY_CODE 
                //                FROM M_ACTIVITYs A 
                //                LEFT JOIN   M_PERIODs P on P.PERIOD_ID = A.PERIOD_ID 
                //                WHERE P.PERIOD_CODE = @PERIOD_CODE";

                //var dbArgsCode = new DynamicParameters();
                //dbArgsCode.Add("PERIOD_CODE", param.PERIOD_CODE);
                //Res.Acode = await Database.QueryAsync<object>(code, dbArgsCode);

                //if (Res.Acode == null)
                //{
                //    param.ACTIVITY_CODE = param.PERIOD_CODE + "-" + "000";
                //}
                #endregion

                string activityC = Database.QueryFirstOrDefault<string>(
                    "SELECT A.ACTIVITY_CODE FROM M_ACTIVITYs A LEFT JOIN   M_PERIODs P on P.PERIOD_ID = A.PERIOD_ID WHERE P.PERIOD_CODE = @PERIOD_CODE order by ACTIVITY_CODE desc",
                    new { PERIOD_CODE = param.PERIOD_CODE });

                if (activityC != null)
                {
                    string[] activityCode = activityC.Split('-');
                    int digitRun = Int32.Parse(activityCode[2]) + 1;
                    activityCode[2] = digitRun.ToString();
                    param.ACTIVITY_CODE = param.PERIOD_CODE + "-" + activityCode[2].PadLeft(3, '0');
                }
                else
                {
                    param.ACTIVITY_CODE = param.PERIOD_CODE + "-" + "001";
                }
                
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_ACTIVITYs WHERE ACTIVITY_CODE = @ACTIVITY_CODE", new { ACTIVITY_CODE = param.ACTIVITY_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Activity Code are duplicate.";
                    return Res;
                }

                # region M_Activity
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_ACTIVITYs]
                                                ([ACTIVITY_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[BRANCH_ID]
                                                ,[SECT_ID]
                                                ,[JOB_ID]
                                                ,[PERIOD_ID]
                                                ,[ACTIVITY_CODE]
                                                ,[ACTIVITY_NAME_TH]
                                                ,[ACTIVITY_NAME_EN]
                                                ,[DESCRIPTION]
                                                ,[TOTAL_BUDGET_AMT]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@ACTIVITY_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,'0'
                                                ,@SECT_ID
                                                ,@JOB_ID
                                                ,@PERIOD_ID
                                                ,@ACTIVITY_CODE
                                                ,@ACTIVITY_NAME_TH
                                                ,@ACTIVITY_NAME_EN
                                                ,@DESCRIPTION
                                                ,@TOTAL_BUDGET_AMT
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ID", ID);
                dbArgs.Add("ACTIVITY_CODE", param.ACTIVITY_CODE);
                dbArgs.Add("ACTIVITY_NAME_TH", param.ACTIVITY_NAME_TH);
                dbArgs.Add("ACTIVITY_NAME_EN", param.ACTIVITY_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("SECT_ID", param.SECT_ID);
                dbArgs.Add("JOB_ID", param.JOB_ID);
                dbArgs.Add("PERIOD_ID", param.PERIOD_ID);
                dbArgs.Add("DESCRIPTION", param.DESCRIPTION);
                dbArgs.Add("TOTAL_BUDGET_AMT", param.TOTAL_BUDGET_AMT);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));

                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;
                #endregion

                #region D_Activity_Group

                string IDD = Guid.NewGuid().ToString().ToUpper();
                #region sqlStringDetail
                string sqlStringDetail = @"INSERT INTO [dbo].[D_ACTIVITY_GROUPs]
                                                          ([ACTIVITY_GROUP_ID]
                                                          ,[ACTIVITY_ID]
                                                          ,[SITE_ID]
                                                          ,[SEQ]
                                                          ,[ACTIVITY_GROUP_CODE]
                                                          ,[ACTIVITY_GROUP_NAME]
                                                          ,[DELETE_STATUS]
                                                          ,[CREATE_DATE]
                                                          ,[CREATE_BY]
                                                          ,[UPDATE_DATE]
                                                          ,[UPDATE_BY]
                                                          ,[REVISION])
                                                    VALUES
                                                          (@ACTIVITY_GROUP_ID
                                                          ,@ACTIVITY_ID
                                                          ,@SITE_ID
                                                          ,'1'
                                                          ,'01'
                                                          ,'Main Group'
                                                          ,0
                                                          ,GETDATE()
                                                          ,@USER_ID
                                                          ,GETDATE()
                                                          ,@USER_ID
                                                          ,0)";
                #endregion
                var dbArgsD = new DynamicParameters();
                dbArgsD.Add("ACTIVITY_GROUP_ID", IDD);
                dbArgsD.Add("ACTIVITY_ID", ID);
                dbArgsD.Add("SITE_ID", param.SITE_ID);
                dbArgsD.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlStringDetail, dbArgsD);
                Res.Result = "OK";
                //Res.Data = IDD;

                #endregion

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> AddActGroup(Field.ActivityAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM D_ACTIVITY_GROUPs WHERE ACTIVITY_GROUP_CODE = @ACTIVITY_GROUP_CODE And ACTIVITY_ID = @ACTIVITY_ID ", new { ACTIVITY_GROUP_CODE = param.ACTIVITY_GROUP_CODE , ACTIVITY_ID = param.ACTIVITY_ID });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Group Code are duplicate.";
                    return Res;
                }
                
                #region D_Activity_Group

                string IDG = Guid.NewGuid().ToString().ToUpper();
                #region sqlStringDetail
                string sqlStringDetail = @"INSERT INTO [dbo].[D_ACTIVITY_GROUPs]
                                                          ([ACTIVITY_GROUP_ID]
                                                          ,[ACTIVITY_ID]
                                                          ,[SITE_ID]
                                                          ,[SEQ]
                                                          ,[ACTIVITY_GROUP_CODE]
                                                          ,[ACTIVITY_GROUP_NAME]
                                                          ,[DELETE_STATUS]
                                                          ,[CREATE_DATE]
                                                          ,[CREATE_BY]
                                                          ,[UPDATE_DATE]
                                                          ,[UPDATE_BY]
                                                          ,[REVISION])
                                                    VALUES
                                                          (@ACTIVITY_GROUP_ID
                                                          ,@ACTIVITY_ID
                                                          ,@SITE_ID
                                                          ,@SEQ
                                                          ,@ACTIVITY_GROUP_CODE
                                                          ,@ACTIVITY_GROUP_NAME
                                                          ,0
                                                          ,GETDATE()
                                                          ,@USER_ID
                                                          ,GETDATE()
                                                          ,@USER_ID
                                                          ,0)";
                #endregion
                var dbArgsD = new DynamicParameters();
                dbArgsD.Add("ACTIVITY_GROUP_ID", IDG);
                dbArgsD.Add("ACTIVITY_ID", param.ACTIVITY_ID);
                dbArgsD.Add("SITE_ID", param.SITE_ID);
                dbArgsD.Add("SEQ", param.SEQG);
                dbArgsD.Add("ACTIVITY_GROUP_CODE", param.ACTIVITY_GROUP_CODE);
                dbArgsD.Add("ACTIVITY_GROUP_NAME", param.ACTIVITY_GROUP_NAME);
                dbArgsD.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlStringDetail, dbArgsD);
                Res.Result = "OK";
                Res.Data = param.ACTIVITY_ID;

                #endregion

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.ActivityAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region M_Activity
                string periodC = Database.QueryFirstOrDefault<string>(
                   "SELECT G.PERIOD_CODE FROM[dbo].[M_ACTIVITYs] A LEFT JOIN[dbo].[M_PERIODs] G on G.[PERIOD_ID] = A.[PERIOD_ID] WHERE  A.ACTIVITY_ID = @ACTIVITY_ID",
                   new { ACTIVITY_ID = param.ACTIVITY_ID });
                if (periodC != param.PERIOD_CODE)
                {
                    string activityC = Database.QueryFirstOrDefault<string>(
                   "SELECT A.ACTIVITY_CODE FROM M_ACTIVITYs A LEFT JOIN   M_PERIODs P on P.PERIOD_ID = A.PERIOD_ID WHERE P.PERIOD_CODE = @PERIOD_CODE order by ACTIVITY_CODE desc",
                   new { PERIOD_CODE = param.PERIOD_CODE });

                    if (activityC != null)
                    {
                        string[] activityCode = activityC.Split('-');
                        int digitRun = Int32.Parse(activityCode[2]) + 1;
                        activityCode[2] = digitRun.ToString();
                        param.ACTIVITY_CODE = param.PERIOD_CODE + "-" + activityCode[2].PadLeft(3, '0');
                    }
                    else
                    {
                        param.ACTIVITY_CODE = param.PERIOD_CODE + "-" + "001";
                    }
                }
               
                string sqlStringM = $@"UPDATE [dbo].[M_ACTIVITYs]
                                       SET [SITE_ID] = @SITE_ID
                                          ,[CORP_ID] = @CORP_ID
                                          ,[BRANCH_ID] = '0'
                                          ,[SECT_ID] = @SECT_ID
                                          ,[JOB_ID] = @JOB_ID
                                          ,[PERIOD_ID] = @PERIOD_ID
                                          ,[ACTIVITY_CODE] = @ACTIVITY_CODE
                                          ,[ACTIVITY_NAME_TH] = @ACTIVITY_NAME_TH
                                          ,[ACTIVITY_NAME_EN] = @ACTIVITY_NAME_EN
                                          ,[DESCRIPTION] = @DESCRIPTION
                                          ,[TOTAL_BUDGET_AMT] = @TOTAL_BUDGET_AMT
                                          ,[DELETE_STATUS] = 0
                                          ,[UPDATE_DATE] = GETDATE()
                                          ,[UPDATE_BY] = @USER_ID
                                          ,[REVISION] = REVISION + 1
                                     WHERE [ACTIVITY_ID] = @ACTIVITY_ID";
                var dbArgsM = new DynamicParameters();
                dbArgsM.Add("ACTIVITY_ID", param.ACTIVITY_ID);
                dbArgsM.Add("ACTIVITY_CODE", param.ACTIVITY_CODE);
                dbArgsM.Add("ACTIVITY_NAME_TH", param.ACTIVITY_NAME_TH);
                dbArgsM.Add("ACTIVITY_NAME_EN", param.ACTIVITY_NAME_EN);
                dbArgsM.Add("SITE_ID", param.SITE_ID);
                dbArgsM.Add("CORP_ID", param.CORP_ID);
                dbArgsM.Add("SECT_ID", param.SECT_ID);
                dbArgsM.Add("JOB_ID", param.JOB_ID);
                dbArgsM.Add("PERIOD_ID", param.PERIOD_ID);
                dbArgsM.Add("DESCRIPTION", param.DESCRIPTION);
                dbArgsM.Add("TOTAL_BUDGET_AMT", param.TOTAL_BUDGET_AMT.Replace(",",""));
                dbArgsM.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlStringM, dbArgsM);
                Res.Result = "OK";
                Res.Data = param.ACTIVITY_ID;
                #endregion

                #region D_Activity
                for (int i = 0; i < param.Detail.Count; i++)
                {

                    if (string.IsNullOrEmpty(param.Detail[i].ACTIVITY_ITEM_ID))
                    {
                        string itemC = Database.QueryFirstOrDefault<string>(
                            "SELECT D.ACTIVITY_ITEM_CODE FROM D_ACTIVITYs D LEFT JOIN   D_ACTIVITY_GROUPs G on G.ACTIVITY_GROUP_ID = D.ACTIVITY_GROUP_ID WHERE  D.ACTIVITY_ID = @ACTIVITY_ID and G.ACTIVITY_GROUP_CODE = @ACTIVITY_GROUP_CODE order by ACTIVITY_ITEM_CODE desc",
                            new { ACTIVITY_ID = param.ACTIVITY_ID, ACTIVITY_GROUP_CODE = param.Detail[i].ACTIVITY_GROUP_CODE });

                        if (itemC != null)
                        {
                            string[] itemCodeD = itemC.Split('-');
                            int digitRun = Int32.Parse(itemCodeD[1]) + 1;
                            itemCodeD[1] = digitRun.ToString();
                            param.Detail[i].ACTIVITY_ITEM_CODE = param.Detail[i].ACTIVITY_GROUP_CODE + "-" + itemCodeD[1].PadLeft(3, '0');
                        }
                        else
                        {
                            param.Detail[i].ACTIVITY_ITEM_CODE = param.Detail[i].ACTIVITY_GROUP_CODE + "-" + "001";
                        }

                        #region D_Activity
                        int dupDetail = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM D_ACTIVITYs WHERE ACTIVITY_ITEM_CODE = @ACTIVITY_ITEM_CODE", new { ACTIVITY_ITEM_CODE = param.Detail[i].ACTIVITY_ITEM_CODE });
                            #region D_Activity
                            string IDD = Guid.NewGuid().ToString().ToUpper();
                            #region sqlStringDetail
                            string sqlStringDetail = @"INSERT INTO [dbo].[D_ACTIVITYs]
                                                            ([ACTIVITY_ITEM_ID]
                                                            ,[ACTIVITY_ID]
                                                            ,[SITE_ID]
                                                            ,[SEQ]
                                                            ,[ACTIVITY_ITEM_CODE]
                                                            ,[ACTIVITY_ITEM_NAME]
                                                            ,[ACTIVITY_GROUP_ID]
                                                            ,[BUDGET_CHART_ID]
                                                            ,[BUDGET_AMT]
                                                            ,[DELETE_STATUS]
                                                            ,[CREATE_DATE]
                                                            ,[CREATE_BY]
                                                            ,[UPDATE_DATE]
                                                            ,[UPDATE_BY]
                                                            ,[REVISION])
                                                       VALUES
                                                            (@ACTIVITY_ITEM_ID
                                                            ,@ACTIVITY_ID
                                                            ,@SITE_ID
                                                            ,@SEQ
                                                            ,@ACTIVITY_ITEM_CODE
                                                            ,@ACTIVITY_ITEM_NAME
                                                            ,@ACTIVITY_GROUP_ID
                                                            ,@BUDGET_CHART_ID
                                                            ,@BUDGET_AMT
                                                            ,0
                                                            ,GETDATE()
                                                            ,@USER_ID
                                                            ,GETDATE()
                                                            ,@USER_ID
                                                            ,0)";
                            #endregion
                            var dbArgsD = new DynamicParameters();
                            dbArgsD.Add("ACTIVITY_ITEM_ID", IDD);
                            dbArgsD.Add("ACTIVITY_ID", param.ACTIVITY_ID);
                            dbArgsD.Add("SITE_ID", param.SITE_ID);
                            dbArgsD.Add("SEQ", param.Detail[i].SEQ);
                            dbArgsD.Add("ACTIVITY_ITEM_CODE", param.Detail[i].ACTIVITY_ITEM_CODE);
                            dbArgsD.Add("ACTIVITY_ITEM_NAME", param.Detail[i].ACTIVITY_ITEM_NAME);
                            dbArgsD.Add("ACTIVITY_GROUP_ID", param.Detail[i].ACTIVITY_GROUP_ID);
                            dbArgsD.Add("BUDGET_CHART_ID", param.Detail[i].BUDGET_CHART_ID);
                            dbArgsD.Add("BUDGET_AMT", param.Detail[i].BUDGET_AMT);
                            dbArgsD.Add("USER_ID", User.FindFirstValue("USER_ID"));
                            await Database.ExecuteAsync(sqlStringDetail, dbArgsD);
                            Res.Result = "OK";

                            #endregion
                        
                        #endregion
                    }
                    else
                    {
                        #region sqlStringDetail
                        string sqlStringDetail = $@" UPDATE D_ACTIVITYs
                                            SET  ACTIVITY_ID = @ACTIVITY_ID
                                                ,SITE_ID = @SITE_ID
                                                ,ACTIVITY_ITEM_CODE = @ACTIVITY_ITEM_CODE
                                                ,ACTIVITY_ITEM_NAME = @ACTIVITY_ITEM_NAME
                                                ,ACTIVITY_GROUP_ID = @ACTIVITY_GROUP_ID
                                                ,BUDGET_CHART_ID = @BUDGET_CHART_ID
                                                ,BUDGET_AMT = @BUDGET_AMT
                                                ,DELETE_STATUS = 0
                                                ,CREATE_DATE = GETDATE()
                                                ,CREATE_BY = @USER_ID
                                                ,UPDATE_DATE = GETDATE()
                                                ,UPDATE_BY = @USER_ID
                                                ,REVISION = REVISION + 1
                                            WHERE ACTIVITY_ITEM_ID = @ACTIVITY_ITEM_ID";
                        #endregion
                        var dbArgsD = new DynamicParameters();
                        dbArgsD.Add("ACTIVITY_ITEM_ID", param.Detail[i].ACTIVITY_ITEM_ID);
                        dbArgsD.Add("ACTIVITY_ID", param.ACTIVITY_ID);
                        dbArgsD.Add("SITE_ID", param.SITE_ID);
                        dbArgsD.Add("ACTIVITY_ITEM_CODE", param.Detail[i].ACTIVITY_ITEM_CODE);
                        dbArgsD.Add("ACTIVITY_ITEM_NAME", param.Detail[i].ACTIVITY_ITEM_NAME);
                        dbArgsD.Add("ACTIVITY_GROUP_ID", param.Detail[i].ACTIVITY_GROUP_ID);
                        dbArgsD.Add("BUDGET_CHART_ID", param.Detail[i].BUDGET_CHART_ID);
                        dbArgsD.Add("BUDGET_AMT", param.Detail[i].BUDGET_AMT);
                        dbArgsD.Add("USER_ID", User.FindFirstValue("USER_ID"));
                        await Database.ExecuteAsync(sqlStringDetail, dbArgsD);
                        Res.Result = "OK";
                    }
                    

                }
                #endregion
            }
            catch (Exception exd)
            {
                Res.Result = "ERROR";
                Res.Message = exd.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
        
        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT   activity.ACTIVITY_ID
                                            , activity.ACTIVITY_CODE
                                            , activity.ACTIVITY_NAME_EN
                                            , activity.ACTIVITY_NAME_TH
                                            , activity.SITE_ID
                                            , activity.CORP_ID
                                            , activity.PERIOD_ID
                                            , activity.SECT_ID
                                            , activity.JOB_ID
                                            , activity.DESCRIPTION
                                            , activity.TOTAL_BUDGET_AMT
                                            , (SELECT TOP 1 PERIOD_CODE FROM M_PERIODs WHERE PERIOD_ID = activity.PERIOD_ID) as PERIOD_CODE
                                            , (SELECT TOP 1 SECT_CODE + ' - ' + SECT_NAME_EN FROM M_SECTs WHERE SECT_ID = activity.SECT_ID) as SECT_CODE
                                            , (SELECT TOP 1 JOB_CODE + ' - ' + JOB_NAME_EN FROM M_JOBs WHERE JOB_ID = activity.JOB_ID) as JOB_CODE
                                            ,sec.DEPT_ID
	                                        , (SELECT TOP 1 D.DEPT_CODE + ' - ' + D.DEPT_NAME_EN FROM M_DEPTs D WHERE D.DEPT_ID = sec.DEPT_ID) as DEPT_CODE
	                                        ,job.PROJ_ID
	                                        ,(SELECT TOP 1 P.PROJ_CODE + ' - ' + P.PROJ_NAME_EN FROM M_PROJs P WHERE P.PROJ_ID = job.PROJ_ID) as PROJ_CODE
                                    FROM M_ACTIVITYs activity 
                                            left join M_SECTs sec on activity.SECT_ID =sec.SECT_ID
                                              LEFT JOIN   M_DEPTs D on D.DEPT_ID = sec.DEPT_ID
                                            left join M_JOBs job on activity.JOB_ID =  job.JOB_ID
                                              LEFT JOIN   M_PROJs P on P.PROJ_ID = job.PROJ_ID
                                    WHERE ACTIVITY_ID = @ACTIVITY_ID";
                #endregion
                #region sqlStringDetail
                string sqlStringDetail = @"SELECT dAct.[ACTIVITY_ITEM_ID]
                                                 ,dAct.[SITE_ID]
                                                 ,dAct.[SEQ]
                                                 ,dAct.[ACTIVITY_ITEM_CODE]
                                                 ,dAct.[ACTIVITY_ITEM_NAME]
                                                 ,dAct.[ACTIVITY_GROUP_ID]
                                                 ,groupA.[ACTIVITY_GROUP_CODE]
                                                 ,dAct.[BUDGET_CHART_ID]
                                                 ,dAct.[BUDGET_AMT]
                                                 ,dAct.[DELETE_STATUS]
                                                 ,dAct.[CREATE_DATE]
                                                 ,dAct.[CREATE_BY]
                                                 ,dAct.[UPDATE_DATE]
                                                 ,dAct.[UPDATE_BY]
                                                 ,dAct.[REVISION]
                                                 ,chart.BUDGET_CHART_CODE
                                            FROM [D_ACTIVITYs] dAct
                                                left join D_ACTIVITY_GROUPs groupA on dAct.ACTIVITY_GROUP_ID =groupA.ACTIVITY_GROUP_ID
                                                left join M_BUDGET_CHARTs chart on dAct.BUDGET_CHART_ID =chart.BUDGET_CHART_ID
                                            WHERE dAct.ACTIVITY_ID = @ACTIVITY_ID ";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ID", ID);
                Res.Header = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Detail = await Database.QueryAsync<object>(sqlStringDetail, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> GetGroup(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT   activity.ACTIVITY_ID
                                            , activity.ACTIVITY_CODE
                                            , activity.ACTIVITY_NAME_EN
                                            , activity.ACTIVITY_NAME_TH
                                            , activity.SITE_ID
                                            , activity.CORP_ID
                                            , activity.PERIOD_ID
                                            , activity.SECT_ID
                                            , activity.JOB_ID
                                            , activity.DESCRIPTION
                                            , activity.TOTAL_BUDGET_AMT
                                            , (SELECT TOP 1 PERIOD_CODE FROM M_PERIODs WHERE PERIOD_ID = activity.PERIOD_ID) as PERIOD_CODE
                                            , (SELECT TOP 1 SECT_CODE + ' - ' + SECT_NAME_EN FROM M_SECTs WHERE SECT_ID = activity.SECT_ID) as SECT_CODE
                                            , (SELECT TOP 1 JOB_CODE + ' - ' + JOB_NAME_EN FROM M_JOBs WHERE JOB_ID = activity.JOB_ID) as JOB_CODE
                                            ,sec.DEPT_ID
	                                        , (SELECT TOP 1 D.DEPT_CODE + ' - ' + D.DEPT_NAME_EN FROM M_DEPTs D WHERE D.DEPT_ID = sec.DEPT_ID) as DEPT_CODE
	                                        ,job.PROJ_ID
	                                        ,(SELECT TOP 1 P.PROJ_CODE + ' - ' + P.PROJ_NAME_EN FROM M_PROJs P WHERE P.PROJ_ID = job.PROJ_ID) as PROJ_CODE
                                    FROM M_ACTIVITYs activity 
                                            left join M_SECTs sec on activity.SECT_ID =sec.SECT_ID
                                              LEFT JOIN   M_DEPTs D on D.DEPT_ID = sec.DEPT_ID
                                            left join M_JOBs job on activity.JOB_ID =  job.JOB_ID
                                              LEFT JOIN   M_PROJs P on P.PROJ_ID = job.PROJ_ID
                                    WHERE ACTIVITY_ID = @ACTIVITY_ID";
                #endregion
                #region sqlStringGroup
                string sqlStringGroup = @"SELECT G.[ACTIVITY_GROUP_ID]
                                                 ,G.[SITE_ID]
                                                 ,G.[SEQ]
                                                 ,G.[ACTIVITY_GROUP_CODE]
                                                 ,G.[ACTIVITY_GROUP_NAME]
                                                 ,G.[DELETE_STATUS]
                                                 ,G.[CREATE_DATE]
                                                 ,G.[CREATE_BY]
                                                 ,G.[UPDATE_DATE]
                                                 ,G.[UPDATE_BY]
                                                 ,G.[REVISION]
                                             FROM [dbo].[D_ACTIVITY_GROUPs] G
                                             WHERE	[ACTIVITY_ID]  = @ACTIVITY_ID ";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ID", ID);
                Res.Header = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Detail = await Database.QueryAsync<object>(sqlStringGroup, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "PERIOD_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.ACTIVITY_ID
                                    		,C.ACTIVITY_CODE
                                    		,C.ACTIVITY_NAME_TH
                                    		,C.ACTIVITY_NAME_EN
                                    		,C.PERIOD_ID
                                    		,C.SECT_ID
                                    		,C.JOB_ID
                                    		,C.TOTAL_BUDGET_AMT
                                    		,S.SITE_ID
                                    		,S.CORP_ID
                                    		,S.PERIOD_CODE
                                    		,S.PERIOD_NAME_EN
                                    		,T.SECT_CODE
                                    		,J.JOB_CODE
			                        		,TT.DEPT_ID
			                        		,D.DEPT_CODE
			                        		,JJ.PROJ_ID
			                        		,P.PROJ_CODE
                                    FROM	M_ACTIVITYs C 
                                    LEFT JOIN   M_PERIODs S on S.PERIOD_ID = C.PERIOD_ID
                                    LEFT JOIN   M_SECTs T on T.SECT_ID = C.SECT_ID
                                    LEFT JOIN   M_JOBs J on J.JOB_ID = C.JOB_ID
                                    , M_SECTs TT
                                    LEFT JOIN   M_DEPTs D on D.DEPT_ID = TT.DEPT_ID
                                    , M_JOBs JJ
                                    LEFT JOIN   M_PROJs P on P.PROJ_ID = JJ.PROJ_ID
                                    WHERE	TT.SECT_ID = C.SECT_ID AND JJ.JOB_ID = C.JOB_ID";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_ACTIVITYs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.ACTIVITY_CODE LIKE @Search OR  C.ACTIVITY_NAME_TH LIKE @Search OR  C.ACTIVITY_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.PERIOD_ID))
                {
                    dbArgs.Add("@Search", param.PERIOD_ID);
                    queryWhereBy += "\n AND C.PERIOD_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.PERIOD_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.PERIOD_CODE , C.ACTIVITY_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> ListGroup(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "ACTIVITY_GROUP_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT G.[ACTIVITY_GROUP_ID]
                                           ,G.[ACTIVITY_ID]
                                           ,G.[SITE_ID]
                                           ,G.[SEQ]
                                           ,G.[ACTIVITY_GROUP_CODE]
                                           ,G.[ACTIVITY_GROUP_NAME]
                                           ,G.[DELETE_STATUS]
                                           ,G.[CREATE_DATE]
                                           ,G.[CREATE_BY]
                                           ,G.[UPDATE_DATE]
                                           ,G.[UPDATE_BY]
                                           ,G.[REVISION]
                                     FROM [dbo].[D_ACTIVITY_GROUPs] G 
                                        LEFT JOIN   M_ACTIVITYs A on A.ACTIVITY_ID = G.ACTIVITY_ID
                                     WHERE	G.DELETE_STATUS = 0";
                  
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	D_ACTIVITY_GROUPs G 
                                    WHERE	G.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND G.ACTIVITY_CODE LIKE @Search OR  G.ACTIVITY_NAME_TH LIKE @Search OR  G.ACTIVITY_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.ACTIVITY_ID))
                {
                    dbArgs.Add("@Search", param.ACTIVITY_ID);
                    queryWhereBy += "\n AND G.ACTIVITY_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY A.ACTIVITY_CODE , [G].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY A.ACTIVITY_CODE , G.ACTIVITY_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        //public async Task<Rest.DATA> ItemCode(Field.ActivityAdd param)
        //{
        //    Rest.DATA Res = new Rest.DATA();
        //    try
        //    {
        //        string itemCode;
        //        string itemC = Database.QueryFirstOrDefault<string>(
        //            "SELECT D.ACTIVITY_ITEM_CODE FROM D_ACTIVITYs D LEFT JOIN   D_ACTIVITY_GROUPs G on G.ACTIVITY_GROUP_ID = D.ACTIVITY_GROUP_ID WHERE  D.ACTIVITY_ID = @ACTIVITY_ID and G.ACTIVITY_GROUP_CODE = @ACTIVITY_GROUP_CODE order by ACTIVITY_ITEM_CODE desc",
        //            new { ACTIVITY_ID = param.ACTIVITY_ID , ACTIVITY_GROUP_CODE = param.ACTIVITY_GROUP_CODE });

        //        if (itemC != null)
        //        {
        //            string[] itemCodeD = itemC.Split('-');
        //            int digitRun = Int32.Parse(itemCodeD[1]) + 1;
        //            itemCodeD[1] = digitRun.ToString();
        //            itemCode = param.ACTIVITY_GROUP_CODE + "-" + itemCodeD[1].PadLeft(3, '0');
        //        }
        //        else
        //        {
        //            itemCode = param.ACTIVITY_GROUP_CODE + "-" + "001";
        //        }

        //        Res.ItemCode = itemCode;
                
        //    }
        //    catch (Exception ex)
        //    {
        //        Res.Result = "ERROR";
        //        Res.Message = ex.Message;
        //    }
        //    finally
        //    {
        //        Database.Close();
        //        GC.Collect();
        //    }
        //    return Res;
        //}  
    }
}
