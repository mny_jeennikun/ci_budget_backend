﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Activity
{
    public partial class Field
    {
        public class ActivityAdd
        {
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_ID { get; set; }
            public string BRANCH_ID { get; set; }
            public string SECT_ID { get; set; }
            public string JOB_ID { get; set; }
            public string PERIOD_ID { get; set; }
            public string PERIOD_CODE { get; set; }
            public string ACTIVITY_CODE { get; set; }
            public string ACTIVITY_NAME_TH { get; set; }
            public string ACTIVITY_NAME_EN { get; set; }
            public string DESCRIPTION { get; set; }
            public string TOTAL_BUDGET_AMT { get; set; }
            public string SEQG { get; set; }
            public string ACTIVITY_GROUP_CODE { get; set; }
            public string ACTIVITY_GROUP_NAME { get; set; }
            public List<ActivityAddDetail> Detail { get; set; }
            public ActivityAddGroup Group { get; set; }
            public Activity Data { get; set; }
            
        }
        public class ActivityAddDetail
        { 
            //Detail
            public string ACTIVITY_ITEM_ID { get; set; }
            public string SEQ { get; set; }
            public string ACTIVITY_ITEM_CODE { get; set; }
            public string ACTIVITY_ITEM_NAME { get; set; }
            public string ACTIVITY_GROUP_ID { get; set; }
            public string ACTIVITY_GROUP_CODE { get; set; }
            public string BUDGET_CHART_ID { get; set; }
            public string BUDGET_AMT { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }

        }

        public class ActivityAddGroup
        {
            //Group
            public string ACTIVITY_GROUP_ID { get; set; }
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string SEQ { get; set; }
            public string ACTIVITY_GROUP_CODE { get; set; }
            public string ACTIVITY_GROUP_NAME { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }

        }

        public class Activity
        {
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_ID { get; set; }
            public string BRANCH_ID { get; set; }
            public string SECT_ID { get; set; }
            public string JOB_ID { get; set; }
            public string PERIOD_ID { get; set; }
            public string ACTIVITY_CODE { get; set; }
            public string ACTIVITY_NAME_TH { get; set; }
            public string ACTIVITY_NAME_EN { get; set; }
            public string DESCRIPTION { get; set; }
            public string TOTAL_BUDGET_AMT { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
            public List<ActivityDetail> Detail { get; set; }
        }

        public class ActivityDetail
        {
            public string ACTIVITY_ITEM_ID { get; set; }
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string SEQ { get; set; }
            public string ACTIVITY_ITEM_CODE { get; set; }
            public string ACTIVITY_ITEM_NAME { get; set; }
            public string ACTIVITY_ITEM_GROUP { get; set; }
            public string BUDGET_CHART_ID { get; set; }
            public string BUDGET_AMT { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }

        }



        public class Table
        {
            public string ACTIVITY_ID { get; set; }
            public string ACTIVITY_CODE { get; set; }
            public string ACTIVITY_NAME_TH { get; set; }
            public string ACTIVITY_NAME_EN { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string PERIOD_ID { get; set; }
            public string ACTIVITY_ID { get; set; }
        }
    }
}
