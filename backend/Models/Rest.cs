﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public partial class Rest
    {
        public class Jtable
        {
            // "OK" or "ERROR"
            public string Result { get; set; } = "ERROR";
            public object Records { get; set; } = null;
            public int TotalRecordCount { get; set; } = 0;
            public string Message { get; set; }
        }

        public class List : Jtable
        {

        }

        public class DATA
        {
            // "OK" or "ERROR"
            public string Result { get; set; } = "ERROR";
            public object Data { get; set; } = null;
            public object Header { get; set; } = null;
            public object Detail { get; set; } = null;
            public object Acode { get; set; } = null;
            public string Message { get; set; }
            public string ItemCode { get; set; }

        }

    }
}
