﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Corp
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.CorpAdd param)
        {
            if (string.IsNullOrEmpty(param.CORP_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.CorpAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_CORPs WHERE CORP_CODE = @CORP_CODE", new { CORP_CODE = param.CORP_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Corp Code are duplicate.";
                    return Res;
                }
                /*if (!User.IsInRole("21"))
                {
                    param.SITE_ID = User.FindFirstValue("SITE_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_CORPs]
                                           ([CORP_ID]
                                           ,[CORP_CODE]
                                           ,[SITE_ID]
                                           ,[CORP_NAME]
                                           ,[CORP_NAME2]
                                           ,[IS_ACTIVE]
                                           ,[IS_ACTIVE_DATE]
                                           ,[DELETE_STATUS]
                                           ,[CREATE_DATE]
                                           ,[CREATE_BY]
                                           ,[UPDATE_DATE]
                                           ,[UPDATE_BY]
                                           ,[REVISION])
                                     VALUES
                                           (@CORP_ID
                                           ,@CORP_CODE
                                           ,@SITE_ID
                                           ,@CORP_NAME
                                           ,@CORP_NAME2
                                           ,'Y'
                                           ,GETDATE()
                                           ,0
                                           ,GETDATE()
                                           ,@USER_ID
                                           ,GETDATE()
                                           ,@USER_ID
                                           ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_ID", ID);
                dbArgs.Add("CORP_CODE", param.CORP_CODE);
                dbArgs.Add("CORP_NAME", param.CORP_NAME);
                dbArgs.Add("CORP_NAME2", param.CORP_NAME2);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.CorpAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_CORPs
                                	SET	CORP_CODE	=	@CORP_CODE,
                                		CORP_NAME	=	@CORP_NAME,
                                		CORP_NAME2	=	@CORP_NAME2,
                                		SITE_ID 	=	@SITE_ID,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE CORP_ID		=	@CORP_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("CORP_CODE", param.CORP_CODE);
                dbArgs.Add("CORP_NAME", param.CORP_NAME);
                dbArgs.Add("CORP_NAME2", param.CORP_NAME2);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.CORP_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.CORP_ID
                                    , R.CORP_CODE
                                    , R.CORP_NAME
                                    , R.CORP_NAME2
                                    , R.SITE_ID
                                    , (SELECT TOP 1 SITE_CODE + ' - ' + SITE_NAME FROM M_SITEs WHERE SITE_ID = R.SITE_ID) as SITE_CODE
                                    FROM M_CORPs R 
                                    WHERE CORP_ID = @CORP_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "CORP_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.CORP_ID
                                    		,C.CORP_CODE
                                    		,C.CORP_NAME
                                    		,C.CORP_NAME2
                                    		,C.SITE_ID
                                    		,S.SITE_CODE
                                    		,S.SITE_NAME
                                    FROM	M_CORPs C
                                    LEFT JOIN   M_SITEs S on S.SITE_ID = C.SITE_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_CORPs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.CORP_CODE LIKE @Search OR  C.CORP_NAME LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.SITE_ID))
                {
                    dbArgs.Add("@Search", param.SITE_ID);
                    queryWhereBy += "\n AND C.SITE_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.SITE_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.SITE_CODE , C.CORP_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
