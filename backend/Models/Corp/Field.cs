﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Corp
{
    public partial class Field
    {
        public class CorpAdd
        {
            public string CORP_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_CODE { get; set; }
            public string CORP_NAME { get; set; }
            public string CORP_NAME2 { get; set; }
        }
        public class Corp
        {
            public string CORP_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_CODE { get; set; }
            public string CORP_NAME { get; set; }
            public string CORP_NAME2 { get; set; }
            public string REFER_ID_1 { get; set; }
            public int IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string CORP_ID { get; set; }
            public string CORP_CODE { get; set; }
            public string CORP_NAME { get; set; }
            public int IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string SITE_ID { get; set; }
        }
    }
}
