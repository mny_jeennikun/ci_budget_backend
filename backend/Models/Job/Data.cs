﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Job
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.JobAdd param)
        {
            if (string.IsNullOrEmpty(param.JOB_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.JobAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_JOBs WHERE JOB_CODE = @JOB_CODE", new { JOB_CODE = param.JOB_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "JOB Code are duplicate.";
                    return Res;
                }
                /*if (!User.IsInRole("21"))
                {
                    param.PROJ_ID = User.FindFirstValue("PROJ_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_JOBs]
                                                ([JOB_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[PROJ_ID]
                                                ,[JOB_CODE]
                                                ,[JOB_NAME_TH]
                                                ,[JOB_NAME_EN]
                                                ,[IS_ACTIVE]
                                                ,[IS_ACTIVE_DATE]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@JOB_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,@PROJ_ID
                                                ,@JOB_CODE
                                                ,@JOB_NAME_TH
                                                ,@JOB_NAME_EN
                                                ,0
                                                ,GETDATE()
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("JOB_ID", ID);
                dbArgs.Add("JOB_CODE", param.JOB_CODE);
                dbArgs.Add("JOB_NAME_TH", param.JOB_NAME_TH);
                dbArgs.Add("JOB_NAME_EN", param.JOB_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("PROJ_ID", param.PROJ_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.JobAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_JOBs
                                	SET	JOB_CODE	=	@JOB_CODE,
                                		JOB_NAME_TH	=	@JOB_NAME_TH,
                                        JOB_NAME_EN	=	@JOB_NAME_EN,
                                        SITE_ID     = @SITE_ID,
                                        CORP_ID     = @CORP_ID,
                                		PROJ_ID     = @PROJ_ID,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE JOB_ID		=	@JOB_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("JOB_ID", param.JOB_ID);
                dbArgs.Add("JOB_CODE", param.JOB_CODE);
                dbArgs.Add("JOB_NAME_TH", param.JOB_NAME_TH);
                dbArgs.Add("JOB_NAME_EN", param.JOB_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("PROJ_ID", param.PROJ_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.JOB_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.JOB_ID
                                    , R.JOB_CODE
                                    , R.JOB_NAME_TH
                                    , R.JOB_NAME_EN
                                    , R.SITE_ID
                                    , R.CORP_ID
                                    , R.PROJ_ID
                                    , (SELECT TOP 1 PROJ_CODE + ' - ' + PROJ_NAME_EN FROM M_PROJs WHERE PROJ_ID = R.PROJ_ID) as PROJ_CODE
                                    FROM M_JOBs R 
                                    WHERE JOB_ID = @JOB_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("JOB_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "PROJ_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.JOB_ID
                                    		,C.JOB_CODE
                                    		,C.JOB_NAME_TH
                                    		,C.JOB_NAME_EN
                                    		,C.PROJ_ID
                                            ,S.SITE_ID
                                            ,S.CORP_ID
                                    		,S.PROJ_CODE
                                    		,S.PROJ_NAME_EN
                                    FROM	M_JOBs C
                                    LEFT JOIN   M_PROJs S on S.PROJ_ID = C.PROJ_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_JOBs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.JOB_CODE LIKE @Search OR  C.JOB_NAME_TH LIKE @Search OR  C.JOB_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.PROJ_ID))
                {
                    dbArgs.Add("@Search", param.PROJ_ID);
                    queryWhereBy += "\n AND C.PROJ_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.PROJ_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.PROJ_CODE , C.JOB_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
