﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.CorpUser
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.CorpUserAdd param)
        {
            if (string.IsNullOrEmpty(param.CORP_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.CorpUserAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO T_CORP_USER
                                                  (CORP_USER_ID
                                                  ,CORP_ID
                                                  ,USER_ID
                                                  ,START_DATE
                                                  ,END_DATE
                                                  ,IS_ACTIVE
                                                  ,DELETE_STATUS
                                                  ,CREATE_DATE
                                                  ,CREATE_BY
                                                  ,UPDATE_DATE
                                                  ,UPDATE_BY
                                                  ,REVISION)
                                            VALUES
                                                  (@CORP_USER_ID
                                                  ,@CORP_ID
                                                  ,@USER_ID
                                                  ,@START_DATE
                                                  ,@END_DATE
                                                  ,'Y'
                                                  ,0
                                                  ,GETDATE()
                                                  ,@UPDATE_BY
                                                  ,GETDATE()
                                                  ,@UPDATE_BY
                                                  ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_USER_ID", ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("START_DATE", param.START_DATE);
                dbArgs.Add("END_DATE", param.END_DATE);
                dbArgs.Add("USER_ID", param.USER_ID);
                dbArgs.Add("UPDATE_BY", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.CorpUserAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  T_CORP_USER
                                	SET	CORP_ID	=	@CORP_ID,
                                		START_DATE	=	@START_DATE,
                                		END_DATE	=	@END_DATE,
                                		USER_ID 	=	@USER_ID,
                                		UPDATE_BY	= @UPDATE_BY,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE CORP_USER_ID		=	@CORP_USER_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_USER_ID", param.CORP_USER_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("START_DATE", param.START_DATE);
                dbArgs.Add("END_DATE", param.END_DATE);
                dbArgs.Add("USER_ID", param.USER_ID);
                dbArgs.Add("UPDATE_BY", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.CORP_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT	T1.CORP_USER_ID
                                    		,T1.CORP_ID
                                    		,T1.USER_ID
                                    		,T1.START_DATE
                                    		,T1.END_DATE
                                    		,T1.IS_ACTIVE
                                    		,T1.IS_ACTIVE_DATE
                                    FROM	T_CORP_USER T1
                                    WHERE	CORP_USER_ID = @CORP_USER_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("CORP_USER_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "CORP_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.CORP_ID
                                    		,C.CORP_CODE
                                    		,C.CORP_NAME
                                    		,C.CORP_NAME2
                                    		,C.SITE_ID
                                    		,S.SITE_CODE
                                    		,S.SITE_NAME
                                    FROM	M_CORPs C
                                    LEFT JOIN   M_SITEs S on S.SITE_ID = C.SITE_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_CORPs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.CORP_CODE LIKE @Search OR  C.CORP_NAME LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.SITE_ID))
                {
                    dbArgs.Add("@Search", param.SITE_ID);
                    queryWhereBy += "\n AND C.SITE_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.SITE_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.SITE_CODE , C.CORP_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
