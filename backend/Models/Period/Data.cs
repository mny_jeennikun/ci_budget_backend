﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Period
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.PeriodAdd param)
        {
            if (string.IsNullOrEmpty(param.PERIOD_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.PeriodAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_PERIODs WHERE PERIOD_CODE = @PERIOD_CODE", new { PERIOD_CODE = param.PERIOD_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Period Code are duplicate.";
                    return Res;
                }
                /*if (User.IsInRole("21"))
                {
                    param.CORP_ID = User.FindFirstValue("CORP_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_PERIODs]
                                                ([PERIOD_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[PERIOD_CODE]
                                                ,[PERIOD_NAME_TH]
                                                ,[PERIOD_NAME_EN]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@PERIOD_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,@PERIOD_CODE
                                                ,@PERIOD_NAME_TH
                                                ,@PERIOD_NAME_EN
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("PERIOD_ID", ID);
                dbArgs.Add("PERIOD_CODE", param.PERIOD_CODE);
                dbArgs.Add("PERIOD_NAME_TH", param.PERIOD_NAME_TH);
                dbArgs.Add("PERIOD_NAME_EN", param.PERIOD_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.PeriodAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_PERIODs
                                	SET	PERIOD_CODE	=	@PERIOD_CODE,
                                		PERIOD_NAME_TH	=	@PERIOD_NAME_TH,
                                        PERIOD_NAME_EN	=	@PERIOD_NAME_EN,
                                        SITE_ID     = @SITE_ID,
                                		CORP_ID     = @CORP_ID,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE PERIOD_ID		=	@PERIOD_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("PERIOD_ID", param.PERIOD_ID);
                dbArgs.Add("PERIOD_CODE", param.PERIOD_CODE);
                dbArgs.Add("PERIOD_NAME_TH", param.PERIOD_NAME_TH);
                dbArgs.Add("PERIOD_NAME_EN", param.PERIOD_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.CORP_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.PERIOD_ID
                                    , R.PERIOD_CODE
                                    , R.PERIOD_NAME_TH
                                    , R.PERIOD_NAME_EN
                                    , R.SITE_ID
                                    , R.CORP_ID 
                                    , (SELECT TOP 1 CORP_CODE + ' - ' + CORP_NAME FROM M_CORPs WHERE CORP_ID = R.CORP_ID) as CORP_CODE
                                    FROM M_PERIODs R 
                                    WHERE PERIOD_ID = @PERIOD_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("PERIOD_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "PERIOD_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.PERIOD_ID
                                    		,C.PERIOD_CODE
                                    		,C.PERIOD_NAME_TH
                                    		,C.PERIOD_NAME_EN
                                            ,C.CORP_ID
                                            ,S.SITE_ID
                                    		,S.CORP_CODE
                                    		,S.CORP_NAME
                                    FROM	M_PERIODs C
                                    LEFT JOIN   M_CORPs S on S.CORP_ID = C.CORP_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_PERIODs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.PERIOD_CODE LIKE @Search OR  C.PERIOD_NAME_TH LIKE @Search OR  C.PERIOD_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.CORP_ID))
                {
                    dbArgs.Add("@Search", param.CORP_ID);
                    queryWhereBy += "\n AND C.CORP_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.CORP_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.CORP_CODE , C.CORP_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
