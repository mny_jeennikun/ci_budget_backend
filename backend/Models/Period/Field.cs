﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Period
{
    public partial class Field
    {
        public class PeriodAdd
        {
            public string PERIOD_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_ID { get; set; }
            public string PERIOD_CODE { get; set; }
            public string PERIOD_NAME_TH { get; set; }
            public string PERIOD_NAME_EN { get; set; }
        }
        public class Period
        {
            public string PERIOD_ID { get; set; }
            public string SITE_ID { get; set; }
            public string CORP_ID { get; set; }
            public string PERIOD_CODE { get; set; }
            public string PERIOD_NAME_TH { get; set; }
            public string PERIOD_NAME_EN { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string PERIOD_ID { get; set; }
            public string PERIOD_CODE { get; set; }
            public string PERIOD_NAME_TH { get; set; }
            public string PERIOD_NAME_EN { get; set; }
            public string IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string CORP_ID { get; set; }
        }
    }
}
