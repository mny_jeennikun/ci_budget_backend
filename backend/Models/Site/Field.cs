﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Site
{
    public partial class Field
    {
        public class SiteAdd
        {
            public string SITE_ID { get; set; }
            public string SITE_CODE { get; set; }
            public string SITE_NAME { get; set; }
        }
        public class Site
        {
            public string SITE_ID { get; set; }
            public string SITE_CODE { get; set; }
            public string SITE_NAME { get; set; }
            public string IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string SITE_ID { get; set; }
            public string SITE_CODE { get; set; }
            public string SITE_NAME { get; set; }
            public string IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string LastID { get; set; }
            public int PageSize { get; set; }
            public string SortBy { get; set; }
        }
    }
}
