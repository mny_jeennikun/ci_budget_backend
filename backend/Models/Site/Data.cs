﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Site
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }
        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.SiteAdd param)
        {
            if (string.IsNullOrEmpty(param.SITE_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.SiteAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_SITEs WHERE SITE_CODE = @SITE_CODE", new { SITE_CODE = param.SITE_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Site Code are duplicate.";
                    return Res;
                }
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_SITEs]
                                           ([SITE_ID]
                                           ,[SITE_CODE]
                                           ,[SITE_NAME]
                                           ,[IS_ACTIVE]
                                           ,[DELETE_STATUS]
                                           ,[CREATE_DATE]
                                           ,[CREATE_BY]
                                           ,[UPDATE_DATE]
                                           ,[UPDATE_BY]
                                           ,[REVISION])
                                     VALUES
                                           (@SITE_ID
                                           ,@SITE_CODE
                                           ,@SITE_NAME
                                           ,0
                                           ,0
                                           ,GETDATE()
                                           ,@USER_ID
                                           ,GETDATE()
                                           ,@USER_ID
                                           ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SITE_ID", ID);
                dbArgs.Add("SITE_CODE", param.SITE_CODE);
                dbArgs.Add("SITE_NAME", param.SITE_NAME);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.SiteAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_SITEs
                                	SET	SITE_CODE	=	@SITE_CODE,
                                		SITE_NAME	=	@SITE_NAME,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE SITE_ID		=	@SITE_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("SITE_CODE", param.SITE_CODE);
                dbArgs.Add("SITE_NAME", param.SITE_NAME);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.SITE_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT SITE_ID
                                    , SITE_CODE
                                    , SITE_NAME
                                    , IS_ACTIVE
                                    , IS_ACTIVE_DATE 
                                    FROM M_SITEs 
                                    WHERE SITE_ID = @SITE_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SITE_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "SITE_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY CREATE_DATE ASC) AS RECORD
                                    		,S.SITE_ID
                                    		,S.SITE_CODE
                                    		,S.SITE_NAME
                                    		,S.IS_ACTIVE
                                    		,S.IS_ACTIVE_DATE
                                    FROM	M_SITEs S
                                    WHERE	S.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_SITEs S
                                    WHERE	S.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND S.SITE_CODE LIKE @Search OR  S.SITE_NAME LIKE @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY [{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY SITE_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
