﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Role
{
    public partial class Field
    {
        public class RoleAdd
        {
            public string ROLE_ID { get; set; }
            public string SITE_ID { get; set; }
            public string ROLE_CODE { get; set; }
            public string ROLE_NAME { get; set; }
        }
        public class Role
        {
            public string ROLE_ID { get; set; }
            public string SITE_ID { get; set; }
            public string ROLE_CODE { get; set; }
            public string ROLE_NAME { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string ROLE_ID { get; set; }
            public string ROLE_CODE { get; set; }
            public string ROLE_NAME { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string SITE_ID { get; set; }
        }
    }
}
