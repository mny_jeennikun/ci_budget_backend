﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Activity_Item
{
    public partial class Field
    {
        public class Activity_ItemAdd
        {
            public string ACTIVITY_ITEM_ID { get; set; }
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string SEQ { get; set; }
            public string ACTIVITY_ITEM_CODE { get; set; }
            public string ACTIVITY_ITEM_NAME { get; set; }
            public string ACTIVITY_ITEM_GROUP { get; set; }
            public string BUDGET_CHART_ID { get; set; }
            public string BUDGET_AMT { get; set; }
          
        }
        public class Activity_Item
        {
            public string ACTIVITY_ITEM_ID { get; set; }
            public string ACTIVITY_ID { get; set; }
            public string SITE_ID { get; set; }
            public string SEQ { get; set; }
            public string ACTIVITY_ITEM_CODE { get; set; }
            public string ACTIVITY_ITEM_NAME { get; set; }
            public string ACTIVITY_ITEM_GROUP { get; set; }
            public string BUDGET_CHART_ID { get; set; }
            public string BUDGET_AMT { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string ACTIVITY_ITEM_ID { get; set; }
            public string ACTIVITY_ITEM_CODE { get; set; }
            public string ACTIVITY_ITEM_NAME { get; set; }
            public string ACTIVITY_ITEM_GROUP { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string DEPT_ID { get; set; }
        }
    }
}
