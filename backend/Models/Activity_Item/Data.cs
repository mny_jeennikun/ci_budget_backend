﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Activity_Item
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.Activity_ItemAdd param)
        {
            if (string.IsNullOrEmpty(param.ACTIVITY_ITEM_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.Activity_ItemAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM D_ACTIVITYs WHERE ACTIVITY_ITEM_CODE = @ACTIVITY_ITEM_CODE", new { ACTIVITY_ITEM_CODE = param.ACTIVITY_ITEM_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "activity_item Code are duplicate.";
                    return Res;
                }
                /*if (!User.IsInRole("21"))
                {
                    param.DEPT_ID = User.FindFirstValue("DEPT_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[D_ACTIVITYs]
                                                ([ACTIVITY_ITEM_ID]
                                                ,[ACTIVITY_ID]
                                                ,[SITE_ID]
                                                ,[SEQ]
                                                ,[ACTIVITY_ITEM_CODE]
                                                ,[ACTIVITY_ITEM_NAME]
                                                ,[ACTIVITY_ITEM_GROUP]
                                                ,[BUDGET_CHART_ID]
                                                ,[BUDGET_AMT]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@ACTIVITY_ITEM_ID
                                                ,@ACTIVITY_ID
                                                ,@SITE_ID
                                                ,@SEQ
                                                ,@ACTIVITY_ITEM_CODE
                                                ,@ACTIVITY_ITEM_NAME
                                                ,@ACTIVITY_ITEM_GROUP
                                                ,@BUDGET_CHART_ID
                                                ,@BUDGET_AMT
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ITEM_ID", ID);
                dbArgs.Add("ACTIVITY_ITEM_CODE", param.ACTIVITY_ITEM_CODE);
                dbArgs.Add("ACTIVITY_ITEM_NAME", param.ACTIVITY_ITEM_NAME);
                dbArgs.Add("ACTIVITY_ITEM_GROUP", param.ACTIVITY_ITEM_GROUP);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("ACTIVITY_ID", param.ACTIVITY_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.Activity_ItemAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  D_ACTIVITYs
                                	SET	ACTIVITY_ITEM_ID = @ACTIVITY_ITEM_ID
                                            ,ACTIVITY_ID = @ACTIVITY_ID
                                            ,SITE_ID = @SITE_ID
                                            ,ACTIVITY_ITEM_CODE = @ACTIVITY_ITEM_CODE
                                            ,ACTIVITY_ITEM_NAME = @ACTIVITY_ITEM_NAME
                                            ,ACTIVITY_ITEM_GROUP = @ACTIVITY_ITEM_GROUP
                                            ,BUDGET_CHART_ID = @BUDGET_CHART_ID
                                            ,BUDGET_AMT = @BUDGET_AMT
                                            ,DELETE_STATUS = @DELETE_STATUS
                                            ,UPDATE_DATE = @UPDATE_DATE
                                            ,UPDATE_BY = @USER_ID
                                            ,REVISION	=	REVISION + 1
                                WHERE ACTIVITY_ITEM_ID		=	@ACTIVITY_ITEM_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ITEM_ID", param.ACTIVITY_ITEM_ID);
                dbArgs.Add("ACTIVITY_ITEM_CODE", param.ACTIVITY_ITEM_CODE);
                dbArgs.Add("ACTIVITY_ITEM_NAME", param.ACTIVITY_ITEM_NAME);
                dbArgs.Add("ACTIVITY_ITEM_GROUP", param.ACTIVITY_ITEM_GROUP);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.ACTIVITY_ITEM_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.ACTIVITY_ITEM_ID
                                    , R.ACTIVITY_ITEM_CODE
                                    , R.ACTIVITY_ITEM_NAME_TH
                                    , R.ACTIVITY_ITEM_NAME_EN
                                    , R.SITE_ID
                                    , R.CORP_ID
                                    , R.DEPT_ID
                                    , (SELECT TOP 1 DEPT_CODE + ' - ' + DEPT_NAME_EN FROM M_DEPTs WHERE DEPT_ID = R.DEPT_ID) as DEPT_CODE
                                    FROM D_ACTIVITYs R 
                                    WHERE ACTIVITY_ITEM_ID = @ACTIVITY_ITEM_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("ACTIVITY_ITEM_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "DEPT_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.ACTIVITY_ITEM_ID
                                    		,C.ACTIVITY_ITEM_CODE
                                    		,C.ACTIVITY_ITEM_NAME_TH
                                    		,C.ACTIVITY_ITEM_NAME_EN
                                    		,C.DEPT_ID
                                            ,S.SITE_ID
                                            ,S.CORP_ID
                                    		,S.DEPT_CODE
                                    		,S.DEPT_NAME_EN
                                    FROM	D_ACTIVITYs C
                                    LEFT JOIN   M_DEPTs S on S.DEPT_ID = C.DEPT_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	D_ACTIVITYs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.ACTIVITY_ITEM_CODE LIKE @Search OR  C.ACTIVITY_ITEM_NAME_TH LIKE @Search OR  C.ACTIVITY_ITEM_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.DEPT_ID))
                {
                    dbArgs.Add("@Search", param.DEPT_ID);
                    queryWhereBy += "\n AND C.DEPT_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.DEPT_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.DEPT_CODE , C.ACTIVITY_ITEM_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
