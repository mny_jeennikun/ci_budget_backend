﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Sect
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.SectAdd param)
        {
            if (string.IsNullOrEmpty(param.SECT_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.SectAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_SECTs WHERE SECT_CODE = @SECT_CODE", new { SECT_CODE = param.SECT_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Sect Code are duplicate.";
                    return Res;
                }
                /*if (!User.IsInRole("21"))
                {
                    param.DEPT_ID = User.FindFirstValue("DEPT_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_SECTs]
                                                ([SECT_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[DEPT_ID]
                                                ,[SECT_CODE]
                                                ,[SECT_NAME_TH]
                                                ,[SECT_NAME_EN]
                                                ,[IS_ACTIVE]
                                                ,[IS_ACTIVE_DATE]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@SECT_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,@DEPT_ID
                                                ,@SECT_CODE
                                                ,@SECT_NAME_TH
                                                ,@SECT_NAME_EN
                                                ,0
                                                ,GETDATE()
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SECT_ID", ID);
                dbArgs.Add("SECT_CODE", param.SECT_CODE);
                dbArgs.Add("SECT_NAME_TH", param.SECT_NAME_TH);
                dbArgs.Add("SECT_NAME_EN", param.SECT_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("DEPT_ID", param.DEPT_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.SectAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_SECTs
                                	SET	SECT_CODE	=	@SECT_CODE,
                                		SECT_NAME_TH	=	@SECT_NAME_TH,
                                        SECT_NAME_EN	=	@SECT_NAME_EN,
                                        SITE_ID     = @SITE_ID,
                                        CORP_ID     = @CORP_ID,
                                		DEPT_ID     = @DEPT_ID,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE SECT_ID		=	@SECT_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SECT_ID", param.SECT_ID);
                dbArgs.Add("SECT_CODE", param.SECT_CODE);
                dbArgs.Add("SECT_NAME_TH", param.SECT_NAME_TH);
                dbArgs.Add("SECT_NAME_EN", param.SECT_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("DEPT_ID", param.DEPT_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.SECT_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.SECT_ID
                                    , R.SECT_CODE
                                    , R.SECT_NAME_TH
                                    , R.SECT_NAME_EN
                                    , R.SITE_ID
                                    , R.CORP_ID
                                    , R.DEPT_ID
                                    , (SELECT TOP 1 DEPT_CODE + ' - ' + DEPT_NAME_EN FROM M_DEPTs WHERE DEPT_ID = R.DEPT_ID) as DEPT_CODE
                                    FROM M_SECTs R 
                                    WHERE SECT_ID = @SECT_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("SECT_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "DEPT_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.SECT_ID
                                    		,C.SECT_CODE
                                    		,C.SECT_NAME_TH
                                    		,C.SECT_NAME_EN
                                    		,C.DEPT_ID
                                            ,S.SITE_ID
                                            ,S.CORP_ID
                                    		,S.DEPT_CODE
                                    		,S.DEPT_NAME_EN
                                    FROM	M_SECTs C
                                    LEFT JOIN   M_DEPTs S on S.DEPT_ID = C.DEPT_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_SECTs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.SECT_CODE LIKE @Search OR  C.SECT_NAME_TH LIKE @Search OR  C.SECT_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.DEPT_ID))
                {
                    dbArgs.Add("@Search", param.DEPT_ID);
                    queryWhereBy += "\n AND C.DEPT_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.DEPT_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.DEPT_CODE , C.SECT_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
