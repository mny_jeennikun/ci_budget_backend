﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Budget_Chart
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.Budget_ChartAdd param)
        {
            if (string.IsNullOrEmpty(param.BUDGET_CHART_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.Budget_ChartAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_BUDGET_CHARTs WHERE BUDGET_CHART_CODE = @BUDGET_CHART_CODE", new { BUDGET_CHART_CODE = param.BUDGET_CHART_CODE });
                if (dup > 0)
                {
                    Res.Result = "ERROR";
                    Res.Message = "Budget_Chart Code are duplicate.";
                    return Res;
                }
                /*if (User.IsInRole("21"))
                {
                    param.CORP_ID = User.FindFirstValue("CORP_ID");
                }*/
                string ID = Guid.NewGuid().ToString().ToUpper();
                #region sqlString
                string sqlString = @"INSERT INTO [dbo].[M_BUDGET_CHARTs]
                                                ([BUDGET_CHART_ID]
                                                ,[SITE_ID]
                                                ,[CORP_ID]
                                                ,[BUDGET_CHART_CODE]
                                                ,[BUDGET_CHART_NAME_TH]
                                                ,[BUDGET_CHART_NAME_EN]
                                                ,[IS_ACTIVE]
                                                ,[IS_ACTIVE_DATE]
                                                ,[DELETE_STATUS]
                                                ,[CREATE_DATE]
                                                ,[CREATE_BY]
                                                ,[UPDATE_DATE]
                                                ,[UPDATE_BY]
                                                ,[REVISION])
                                          VALUES
                                                (@BUDGET_CHART_ID
                                                ,@SITE_ID
                                                ,@CORP_ID
                                                ,@BUDGET_CHART_CODE
                                                ,@BUDGET_CHART_NAME_TH
                                                ,@BUDGET_CHART_NAME_EN
                                                ,0
                                                ,GETDATE()
                                                ,0
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,GETDATE()
                                                ,@USER_ID
                                                ,0)";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("BUDGET_CHART_ID", ID);
                dbArgs.Add("BUDGET_CHART_CODE", param.BUDGET_CHART_CODE);
                dbArgs.Add("BUDGET_CHART_NAME_TH", param.BUDGET_CHART_NAME_TH);
                dbArgs.Add("BUDGET_CHART_NAME_EN", param.BUDGET_CHART_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Update(Field.Budget_ChartAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_Budget_Charts
                                	SET	BUDGET_CHART_CODE	=	@BUDGET_CHART_CODE,
                                		BUDGET_CHART_NAME_TH	=	@BUDGET_CHART_NAME_TH,
                                        BUDGET_CHART_NAME_EN	=	@BUDGET_CHART_NAME_EN,
                                        SITE_ID     = @SITE_ID,
                                		CORP_ID     = @CORP_ID,
                                		UPDATE_BY	= @USER_ID,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE BUDGET_CHART_ID		=	@BUDGET_CHART_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("BUDGET_CHART_ID", param.BUDGET_CHART_ID);
                dbArgs.Add("BUDGET_CHART_CODE", param.BUDGET_CHART_CODE);
                dbArgs.Add("BUDGET_CHART_NAME_TH", param.BUDGET_CHART_NAME_TH);
                dbArgs.Add("BUDGET_CHART_NAME_EN", param.BUDGET_CHART_NAME_EN);
                dbArgs.Add("SITE_ID", param.SITE_ID);
                dbArgs.Add("CORP_ID", param.CORP_ID);
                dbArgs.Add("USER_ID", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.CORP_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.BUDGET_CHART_ID
                                    , R.BUDGET_CHART_CODE
                                    , R.BUDGET_CHART_NAME_TH
                                    , R.BUDGET_CHART_NAME_EN
                                    , R.SITE_ID
                                    , R.CORP_ID 
                                    , (SELECT TOP 1 CORP_CODE + ' - ' + CORP_NAME FROM M_CORPs WHERE CORP_ID = R.CORP_ID) as CORP_CODE
                                    FROM M_BUDGET_CHARTs R 
                                    WHERE BUDGET_CHART_ID = @BUDGET_CHART_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("BUDGET_CHART_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "BUDGET_CHART_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.BUDGET_CHART_ID
                                    		,C.BUDGET_CHART_CODE
                                    		,C.BUDGET_CHART_NAME_TH
                                    		,C.BUDGET_CHART_NAME_EN
                                            ,C.CORP_ID
                                            ,S.SITE_ID
                                    		,S.CORP_CODE
                                    		,S.CORP_NAME
                                    FROM	M_BUDGET_CHARTs C
                                    LEFT JOIN   M_CORPs S on S.CORP_ID = C.CORP_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_BUDGET_CHARTs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.BUDGET_CHART_CODE LIKE @Search OR  C.BUDGET_CHART_NAME_TH LIKE @Search OR  C.BUDGET_CHART_NAME_EN LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.CORP_ID))
                {
                    dbArgs.Add("@Search", param.CORP_ID);
                    queryWhereBy += "\n AND C.CORP_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.CORP_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.CORP_CODE , C.CORP_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
