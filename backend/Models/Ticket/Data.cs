﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace backend.Models.Ticket
{
    public partial class Data
    {
        private ClaimsPrincipal User;
        private IDbConnection Database;
        private void Initial()
        {
            Database = new SqlConnection(Startup._connectionString);
        }
        public Data() => Initial();

        public Data(ClaimsPrincipal user)
        {
            Initial();
            this.User = user;
        }

        ~Data()
        {
            User = null;
            Database = null;
            GC.Collect();

        }

        public async Task<Rest.DATA> Save(Field.TicketAdd param)
        {
            if (string.IsNullOrEmpty(param.TICKET_ID))
            {
                return await Add(param);
            }
            return await Update(param);
        }

        public async Task<Rest.DATA> Add(Field.TicketAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            Database.Open();
            int dup = Database.QueryFirstOrDefault<int>("SELECT COUNT(*) FROM M_TICKETs WHERE TICKET_NO = @TICKET_NO", new { TICKET_NO = param.TICKET_NO });
            if (dup > 0)
            {
                Res.Result = "ERROR";
                Res.Message = "Ticket No are duplicate.";
                Database.Close();
                return Res;
            }
            using (var trans = Database.BeginTransaction())
            {
                try
                {
                    string ID = Guid.NewGuid().ToString().ToUpper();
                    string running = await TicketRunning(Database, trans);
                    if (string.IsNullOrEmpty(running))
                    {
                        Res.Result = "ERROR";
                        Res.Message = "Try Again, Cannot Get Ticket No.";
                        trans.Rollback();
                        Database.Close();
                        return Res;
                    }
                    #region sqlString
                    string sqlString = @"INSERT INTO M_TICKETs
                                           (TICKET_ID
                                           ,TICKET_NO
                                           ,TICKET_DATE
                                           ,TICKET_STATUS_ID
                                           ,HR_START_DATE
                                           ,HR_END_DATE
                                           ,HR_SUM_DAY
                                           ,HR_SUM_MINUTE
                                           ,HR_DESCRIPTION
                                           ,DELETE_STATUS
                                           ,CREATE_DATE
                                           ,CREATE_BY
                                           ,UPDATE_DATE
                                           ,UPDATE_BY
                                           ,REVISION)
                                     VALUES
                                           (@TICKET_ID
                                           ,@TICKET_NO
                                           ,@TICKET_DATE
                                           ,@TICKET_STATUS_ID
                                           ,@HR_START_DATE
                                           ,@HR_END_DATE
                                           ,@HR_SUM_DAY
                                           ,@HR_SUM_MINUTE
                                           ,@HR_DESCRIPTION
                                           ,0
                                           ,GETDATE()
                                           ,@UPDATE_BY
                                           ,GETDATE()
                                           ,@UPDATE_BY
                                           ,0)";
                    #endregion
                    var dbArgs = new DynamicParameters();
                    dbArgs.Add("TICKET_ID", ID);
                    dbArgs.Add("TICKET_NO", running);
                    dbArgs.Add("TICKET_DATE", param.TICKET_DATE);
                    dbArgs.Add("HR_START_DATE", param.HR_START_DATE);
                    dbArgs.Add("HR_END_DATE", param.HR_END_DATE);
                    dbArgs.Add("HR_SUM_DAY", param.HR_SUM_DAY);
                    dbArgs.Add("HR_SUM_MINUTE", param.HR_SUM_MINUTE);
                    dbArgs.Add("HR_DESCRIPTION", param.HR_DESCRIPTION);
                    dbArgs.Add("UPDATE_BY", User.FindFirstValue("USER_ID"));
                    await Database.ExecuteAsync(sqlString, dbArgs, trans);
                    trans.Commit();
                    Res.Result = "OK";
                    Res.Data = ID;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    Res.Result = "ERROR";
                    Res.Message = ex.Message;
                }
                finally
                {
                    trans.Dispose();
                    Database.Close();
                    GC.Collect();
                }
            }
            return Res;
        }

        public async Task<string> TicketRunning(IDbConnection db, IDbTransaction trans)
        {
            string running = string.Empty;
            #region Query
            string query = @"SELECT	MAX(TICKET_NO) as RUNNING
                                    FROM		M_TICKETs
                                    WHERE	TICKET_NO LIKE CONVERT(varchar, YEAR(GETDATE())) + RIGHT('0' + CONVERT(varchar, MONTH(GETDATE()) ), 2) +'%'";
            #endregion
            running = db.QueryFirstOrDefault(query, trans);
            if (string.IsNullOrEmpty(running))
            {
                running = DateTime.Now.ToString("yyyyMM") + "000001";
            }
            else
            {
                int newRunning;
                if (int.TryParse(running, out newRunning))
                {
                    newRunning++;
                    running = newRunning.ToString();
                }
            }
            return await Task.FromResult<string>(running);
        }

        public async Task<Rest.DATA> Update(Field.TicketAdd param)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = $@"UPDATE  M_TICKETs
                                	SET	TICKET_NO	=	@TICKET_NO,
                                		TICKET_DATE	=	@TICKET_DATE,
                                		HR_START_DATE 	=	@HR_START_DATE,
                                		HR_END_DATE 	=	@HR_END_DATE,
                                		HR_SUM_DAY 	=	@HR_SUM_DAY,
                                		HR_SUM_MINUTE 	=	@HR_SUM_MINUTE,
                                		HR_DESCRIPTION 	=	@HR_DESCRIPTION,
                                		UPDATE_BY	= @UPDATE_BY,
                                		UPDATE_DATE	= GETDATE(),
                                		REVISION	=	REVISION + 1
                                WHERE TICKET_ID		=	@TICKET_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("TICKET_ID", param.TICKET_ID);
                dbArgs.Add("TICKET_NO", param.TICKET_NO);
                dbArgs.Add("TICKET_DATE", param.TICKET_DATE);
                dbArgs.Add("HR_START_DATE", param.HR_START_DATE);
                dbArgs.Add("HR_END_DATE", param.HR_END_DATE);
                dbArgs.Add("HR_SUM_DAY", param.HR_SUM_DAY);
                dbArgs.Add("HR_SUM_MINUTE", param.HR_SUM_MINUTE);
                dbArgs.Add("HR_DESCRIPTION", param.HR_DESCRIPTION);
                dbArgs.Add("UPDATE_BY", User.FindFirstValue("USER_ID"));
                await Database.ExecuteAsync(sqlString, dbArgs);
                Res.Result = "OK";
                Res.Data = param.TICKET_ID;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.DATA> Get(string ID)
        {
            Rest.DATA Res = new Rest.DATA();
            try
            {
                #region sqlString
                string sqlString = @"SELECT R.TICKET_ID
                                    , R.TICKET_CODE
                                    , R.TICKET_NAME
                                    , R.TICKET_NAME2
                                    , R.SITE_ID
                                    , (SELECT TOP 1 SITE_CODE + ' - ' + SITE_NAME FROM M_SITEs WHERE SITE_ID = R.SITE_ID) as SITE_CODE
                                    FROM M_TICKETs R 
                                    WHERE TICKET_ID = @TICKET_ID";
                #endregion
                var dbArgs = new DynamicParameters();
                dbArgs.Add("TICKET_ID", ID);
                Res.Data = await Database.QueryFirstOrDefaultAsync<object>(sqlString, dbArgs);
                Res.Result = "OK";

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }

        public async Task<Rest.Jtable> List(Field.Filter param, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = "TICKET_CODE ASC")
        {
            Rest.Jtable Res = new Rest.Jtable();
            string[] sortby = (string[])null;
            try
            {
                var dbArgs = new DynamicParameters();
                string queryWhereBy = string.Empty;
                string queryList = @"SELECT	 ROW_NUMBER() OVER(ORDER BY C.CREATE_DATE ASC) AS RECORD
                                    		,C.TICKET_ID
                                    		,C.TICKET_NO
                                    		,C.TICKET_DATE
                                    		,C.HR_START_DATE
                                    		,C.HR_END_DATE
                                    		,C.HR_SUM_DAY
                                    		,C.HR_SUM_MINUTE
                                    		,T.TICKET_TYPE_NAME
                                    		,S.TICKET_STATUS_NAME
                                    FROM	M_TICKETs C
                                    LEFT JOIN   M_TICKET_TYPE T on T.TICKET_TYPE_ID = C.TICKET_TYPE_ID
                                    LEFT JOIN   M_TICKET_STATUS S on S.TICKET_STATUS_ID = C.TICKET_STATUS_ID
                                    WHERE	C.DELETE_STATUS = 0";
                string queryCount = @"SELECT COUNT(*) 
                                    FROM	M_TICKETs C
                                    WHERE	C.DELETE_STATUS = 0";
                if (!string.IsNullOrEmpty(param.Search))
                {
                    dbArgs.Add("@Search", $"%{param.Search }%");
                    queryWhereBy += "\n AND C.TICKET_CODE LIKE @Search OR  C.TICKET_NAME LIKE @Search";
                }
                if (!string.IsNullOrEmpty(param.SITE_ID))
                {
                    dbArgs.Add("@Search", param.SITE_ID);
                    queryWhereBy += "\n AND C.SITE_ID = @Search";
                }
                Res.TotalRecordCount = await Database.QuerySingleAsync<int>(queryCount + queryWhereBy, dbArgs);
                queryList += queryWhereBy;
                if (!string.IsNullOrEmpty(jtSorting))
                {
                    sortby = jtSorting.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                    queryList += $" ORDER BY S.SITE_CODE , [C].[{sortby[0]}] {sortby[1]}";
                }
                else
                {
                    queryList += " ORDER BY S.SITE_CODE , C.TICKET_CODE ASC";
                }
                if (jtPageSize > 0)
                {
                    queryList += " OFFSET @jtStartIndex rows FETCH NEXT @jtPageSize rows only;";
                    dbArgs.Add("@jtPageSize", jtPageSize);
                    dbArgs.Add("@jtStartIndex", jtStartIndex);
                }
                var data = await Database.QueryAsync(queryList, dbArgs);
                Res.Result = "OK";
                Res.Records = data;

            }
            catch (Exception ex)
            {
                Res.Result = "ERROR";
                Res.Message = ex.Message;
            }
            finally
            {
                Database.Close();
                GC.Collect();
            }
            return Res;
        }
    }
}
