﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models.Ticket
{
    public partial class Field
    {
        public class TicketAdd
        {
            public string TICKET_ID { get; set; }
            public string  TICKET_NO { get; set; }
            public string TICKET_DATE { get; set; }
            public string TICKET_STATUS_ID { get; set; }
            public DateTime HR_START_DATE { get; set; }
            public DateTime HR_END_DATE { get; set; }
            public Decimal HR_SUM_DAY { get; set; }
            public Decimal HR_SUM_MINUTE { get; set; }
            public string HR_DESCRIPTION { get; set; }
        }
        public class Ticket
        {
            public string TICKET_ID { get; set; }
            public string TICKET_NO { get; set; }
            public string TICKET_DATE { get; set; }
            public string TICKET_STATUS_ID { get; set; }
            public DateTime HR_START_DATE { get; set; }
            public DateTime HR_END_DATE { get; set; }
            public Decimal HR_SUM_DAY { get; set; }
            public Decimal HR_SUM_MINUTE { get; set; }
            public string HR_DESCRIPTION { get; set; }
            public int DELETE_STATUS { get; set; }
            public string CREATE_BY { get; set; }
            public DateTime CREATE_DATE { get; set; }
            public string UPDATE_BY { get; set; }
            public DateTime UPDATE_DATE { get; set; }
            public int REVISION { get; set; }
        }



        public class Table
        {
            public string TICKET_ID { get; set; }
            public string TICKET_CODE { get; set; }
            public string TICKET_NAME { get; set; }
            public int IS_ACTIVE { get; set; }
            public DateTime? IS_ACTIVE_DATE { get; set; }
        }
        public class Filter
        {
            public string Search { get; set; }
            public string SITE_ID { get; set; }
        }
    }
}
