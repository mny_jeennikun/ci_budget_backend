using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using backend.Models.Activity;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Threading;

namespace backend.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Activity")]
    public class ActivityController : Controller
    {
        [HttpGet("{ID}")]
        public async Task<JsonResult> GetDeatil(string ID)
        {
            return Json(await new Data().Get(ID));
        }

        [HttpGet("Group/{ID}")]
        public async Task<JsonResult> GetGroup(string ID)
        {
            return Json(await new Data().GetGroup(ID));
        }

        [HttpPost("Save")]
        public async Task<JsonResult> Save(Field.ActivityAdd param)
        //public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken) Save(Field.ActivityAdd param)
        {
            return Json(await new Data(User).Save(param));
            //return Json(new Data(User).Save(param));
        }

        [HttpPost("AddGroup")]
        public async Task<JsonResult> AddGroup(Field.ActivityAdd param)
        {
            return Json(await new Data(User).AddGroup(param));
        }
        //[HttpPost("ItemCode")]
        //public async Task<JsonResult> ItemCode(Field.ActivityAdd param)
        //{
        //    return Json(await new Data(User).ItemCode(param));
        //}

        [HttpPost("AddActGroup")]
        public async Task<JsonResult> AddActGroup(Field.ActivityAdd param)
        {
            return Json(await new Data(User).AddActGroup(param));
        }

        [HttpPost("List")]
        public async Task<JsonResult> List(Field.Filter param, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            return Json(await new Data(User).List(param, jtStartIndex, jtPageSize, jtSorting));
        }

        [HttpPost("ListGroup")]
        public async Task<JsonResult> ListGroup(Field.Filter param, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            return Json(await new Data(User).ListGroup(param, jtStartIndex, jtPageSize, jtSorting));
        }

        
    }
}