using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using backend.Models.Role;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : Controller
    {
        [HttpGet("{ID}")]
        public async Task<JsonResult> GetDeatil(string ID)
        {
            return Json(await new Data().Get(ID));
        }

        [HttpPost("Save")]
        public async Task<JsonResult> Save(Field.RoleAdd param)
        {
            return Json(await new Data(User).Save(param));
        }

        [HttpPost("List")]
        public async Task<JsonResult> List(Field.Filter param, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            return Json(await new Data(User).List(param, jtStartIndex, jtPageSize, jtSorting));
        }
    }
}