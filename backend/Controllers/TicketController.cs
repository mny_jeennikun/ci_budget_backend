using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using backend.Models.Ticket;
using Microsoft.AspNetCore.Authorization;

namespace backend.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Ticket")]
    public class TicketController : Controller
    {
        [HttpGet("{ID}")]
        public async Task<JsonResult> GetDeatil(string ID)
        {
            return Json(await new Data().Get(ID));
        }
        /// <summary>
        /// Add new student
        /// </summary>
        /// <param name="student">Student Model</param>
        /// <remarks>Insert new student</remarks>
        /// <response code="400">Bad request</response>
        /// <response code="500">Internal Server Error</response>tpPost("Save")]
        [HttpPost("Save")]
        public async Task<JsonResult> Save([FromForm]Field.TicketAdd param)
        {
            return Json(await new Data(User).Save(param));
        }

        [HttpPost("List")]
        public async Task<JsonResult> List([FromForm]Field.Filter param, int jtStartIndex, int jtPageSize, string jtSorting)
        {
            return Json(await new Data(User).List(param, jtStartIndex, jtPageSize, jtSorting));
        }
    }
}