using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using backend.Models.User;

namespace backend.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        [HttpGet("{ID}")]
        public async Task<JsonResult> GetDeatil(string ID)
        {
            return Json(await new Data().GetUser(ID));
        }
        [HttpPost("Login")]
        public async Task<JsonResult> Login(Field.Login param)
        {
            return Json(await Task.FromResult(new { }));
        }

        [HttpPost("ForgetPassword")]
        public async Task<JsonResult> ForgetPassword([FromForm]string USER_EMAIL)
        {
            return Json(await new Data().ForgetPassword(USER_EMAIL));
        }


    }
}